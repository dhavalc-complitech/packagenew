<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */

//Route::get('/',  ['uses' => 'UserController@index']);
//Route::get('/', 'App\Http\Controllers\UserController@index');

use Simplexi\Greetr\Greetr;

Route::get('/{name}', function($sName) {
    $oGreetr = new Greetr();
    return $oGreetr->greet($sName);
});